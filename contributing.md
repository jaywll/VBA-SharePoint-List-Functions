Contributing to VBA SharePoint List Library
==========================

Authoritative Repository
------------------------

VBA SharePoint List Library is hosted on gitlab.com. The authoritative repository for VBA SharePoint List Library can be found here:
[https://gitlab.com/jaywll/VBA-SharePoint-List-Functions](https://gitlab.com/jaywll/VBA-SharePoint-List-Functions)

If you wish to contribute code to VBA SharePoint List Library, you will need an account on gitlab.com. It's a free service, open to all.


Contribution Process
--------------------

Once you have a gitlab.com account, here are the steps to making a contribution to VBA SharePoint List Library's codebase.

### Step One: Fork and Clone Locally ###
From the [project page on gitlab](https://gitlab.com/jaywll/VBA-SharePoint-List-Functions), click the "fork" button. This creates a copy of the repository within your own gitlab account. Next, clone your repository locally. 

### Step Two: Branch ###
Create a feature branch, check it out, and hack away!

### Step Three: Commit and Push ###
Commit the changes you've made, and push the feature branch back to origin (your copy of the repository on gitlab.com).

### Step Four: Merge Request ###
Back on your gitlab.com account, within your forked repository, select merge requests and submit a request to merge your feature branch (`username/VBA-SharePoint-List-Functions/branchname`) with the project's master branch (`jaywll/VBA-SharePoint-List-Functions/master`).

Don't delete your repository yet! Keep an eye on the comments associated with your merge request. There may be further steps requested of you, such as rebasing your feature branch on `jaywll/VBA-SharePoint-List-Functions/master`.


GitLab Flow
-----------

VBA SharePoint List Library uses a cut-down version of [gitlab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/). As per the instructions above, development takes place on feature branches, and these are merged into the `master` branch when that body work represented by the feature branch is complete.

Periodically, the `master` branch is merged into the `release` branch, and these merge commits are tagged with an appropriate version number. These releases occur at the discretion of the project owner: you should not request to merge a feature branch into `release`, and requests to do so will be rejected.