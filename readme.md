# VBA SharePoint List Library #

VBA SharePoint List Library will be a collection of VBA functions designed to allow your VBA code to interact with a SharePoint list.

Currently it can create new list items, and update existing list items based on their ID. At present there is a single function: `SPListItem`

## Prerequisites ##
- Microsoft Scripting Runtime
- Microsoft XML, v6.0

In the VBA editor window choose Tools -> References.

## Usage ##
`SPListItem(SharePointUrl As String, ListName As String, ListData As Dictionary [, ItemID As Integer])`

- SharePointUrl is the address of a SharePoint site (e.g. http://sharepoint/site/)
- ListName is the name or GUID of a list on that site (e.g. MyList)
- ListData is a VBA dictionary object containing key value pairs, where the key is the name of a field and the value is the value that will be assigned to it.
- ItemID is the SharePoint ID of a list item that will be updated (if omitted, a new list item will be created).

## Contributing ##
Please see the contribution guide in contributing.md